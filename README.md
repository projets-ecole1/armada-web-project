Projet Web Armada
=================== 
![CI status](https://img.shields.io/badge/language-HTML-orange.svg) ![CI status](https://img.shields.io/badge/language-JavaScript-yellow.svg) ![CI status](https://img.shields.io/badge/language-CSS-green.svg) ![CI status](https://img.shields.io/badge/laguage-Bootstrap-%23e242f4.svg)'![CI status](https://img.shields.io/badge/laguage-PHP-blue.svg)

**Contexte**

Pour l'Armada de Juin à Rouen, projet étudiant consistant à réaliser un site web permettant au public d'acceder facilement aux informations des différents bateaux. Pour cela il est
nécessaire que les différents acteurs puissent avoir un accès privilégié.

 - Un visiteur qui valide le formulaire d'inscription se connectera obligatoirement
en tant qu'inscrit. Il a la possibilité d'accéder à plus d'information que quelqu'un
qui n'a pas de compte.
 - Un administrateur du site web à la possibilité de changer le rôle d'un utilisateur
inscrit. Soit en responsable de bateau, soit en un autre administrateur.
 - Le responsable de bateau à la possibilité de rentrer des informations dans un
formulaire : caractéristiques du bateau, une image, un document imprimable de
type PDF contenant des informations plus détaillées, ainsi que les dates d'arrivée
et de départ en juin prochain.
 - Les caractéristiques des bateaux sont accessibles à tous. En revanche il faut être
un utilisateur inscrit pour pouvoir télécharger les PDF contenant les informations
détaillées.

**Cas d'utilisation**

![](https://lh3.googleusercontent.com/xBtXZ2KlLRyMR5rPfJXxe4o6AgTeDLXJrvJxAfJAQZNo-pbgdKs9suBSXwM1LjoQZPq2F0HUnhV8=s600)

**Membres**

> ADJOVI Amiel & ADELEKE Mazidatou