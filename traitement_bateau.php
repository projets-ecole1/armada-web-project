<?php
    $nom_bat = $pays = $greement = $date_arr = $date_dep = $fichier = $fichiers = $taille = "";
    $errNom_bat = $errTaille = $errPays = $errGreement = $errDate_arr = $errDate_dep = $errImage = $errDocument = "";
    $success = $success_i = $success_d = true;
    include("param.inc.php");

    if(isset($_POST["create"])) 
    {

        // Verifie les images et documents d'abord
        include("traitement_image.php");

        include("traitement_documents.php");

         // Verifie si le nom est bien entré
        if (empty($_POST["nom_bat"])) {
            $errNom_bat = "* Veuillez indiquer le nom du bateau!";
            $success = false;
        } else {
            $nom_bat = test_input($_POST["nom_bat"]);
            // Verifie si les caracteres sont correctes
            if (!preg_match("/^[a-zA-Z ]*$/",$nom_bat)) {
              $errNom_bat = "* Seul les lettres et l'espace sont autorisés"; 
              $success = false;
            }
        }
        
        if (empty($_POST["taille"])) {
            $errTaille = "* Veuillez indiquer la taille du bateau";
            $success = false;
        } else {
            $taille = $_POST["taille"];
        }

        if (empty($_POST["pays"])) {
            $errPays = "* Veuillez indiquer le pays!";
            $success = false;
        } else {
            $pays = $_POST["pays"];
        }

        if (empty($_POST["greement"])) {
            $errGreement = "* Veuillez renseigner ce champ!";
            $success = false;
        } else {
            $greement = strip_tags($_POST['greement']);
            $greement = escape_string($greement);
        }

        if (empty($_POST["date_arr"])) {
            $errDate_arr = "* Veuillez indiquer la date d'arrivée!";
            $success = false;
        } else {
            $date_arr = $_POST["date_arr"];
        }

        if (empty($_POST["date_dep"])) {
            $errDate_dep = "* Veuillez indiquer la date de départ!";
            $success = false;
        } else {
            $date_dep = $_POST["date_dep"];
        }

        if($success == true && $success_i == true && $success_d == true && $_FILES["image"]["error"] == 0){
            $fichiers = clean($fichiers); //Remplace tout les caracteres accentués
            $fichiers = preg_replace('/([^.a-z0-9]+)/i', '-', $fichiers); //Remplace tout les caracteres non alphanumeriques par '-'
            $envoie = move_uploaded_file($_FILES["image"]["tmp_name"], $dossier2.$fichiers);
        }else{
            $errImage = " * Erreur d'enregistrement de l'image!";
            $success = false;
        }

        if($success == true && $success_i == true && $success_d == true && $_FILES["document"]["error"] == 0){
            $fichier = clean($fichier); //Remplace tout les caracteres accentués
            $fichier = preg_replace('/([^.a-z0-9]+)/i','-', $fichier); //Remplace tout les caracteres non alphanumeriques par '-'
            $envoie = move_uploaded_file($_FILES["document"]["tmp_name"], $dossier.$fichier);
        }
        else{
            $errDocument;
            $success = false;
        }
    
        if($success == true && $success_i == true && $success_d == true) 
        {
            $conn = mysqli_connect($servername, $username, $mdp, $myDataBaseName); // Create connection
            // Check connection
            if (!$conn){
               die("Connection failed: " . mysqli_connect_error());
            }

            $sql = "INSERT INTO bateaux (nom,pays,taille,greement,date_depart,date_darrivee,image,document,id_respo) VALUES ('$nom_bat','$pays','$taille','$greement','$date_arr','$date_dep','$fichiers','$fichier','$id_respo')";
            if (!mysqli_query($conn, $sql)) {
            echo "Error: " . $sql . "<br>" . mysqli_error($conn);
            }
           
            echo '<div style = "margin-top: 10px" class="alert alert-success alert-dismissible fade show" role="alert">';
            echo '<strong>Bateau Crée!</strong>!';
            echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
            echo '<span aria-hidden="true">&times;</span>';
            echo '</button>';
            echo '</div>';
        }   
    }
 
    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
      }


    function escape_string($string) {
         return str_replace('\'', '\'\'', $string);
    }

    function clean($string){
        // Valeur a nettoyer (conversion)
        $characteres = array(    'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
                                    'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
                                    'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
                                    'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
                                    'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y',
                                    ' ' => '', '_' => '', '-' => '', ',' => '', ';' => '');

        return mb_strtolower(strtr($string, $characteres ));
    }
?> 