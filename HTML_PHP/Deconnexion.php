
<?php session_start(); ?>
<!DOCTYPE html>
<html>
    <body>
        <?php
        // remove all session variables
        session_unset(); 
        // destroy the session 
        session_destroy();

        header("Status: 301 Moved Permanently", false, 301);
        header('Location:/test/armada-web-project/HTML_PHP/Connexion.php?mes=dcon');
        exit();
        ?>
           
    </body>
</html>


