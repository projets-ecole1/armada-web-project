<header>
      <!--Image d'en-tete-->
    <div class="container">
          <img id= "haut" src="/test/armada-web-project/Images/head.png" class="img-fluid" alt="Image Responsive">
      
      <!--Barre de Navigation-->
      <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #c2e5fd;" >
        <img src="/test/armada-web-project/Images/anchor.png" width="30" height="30" class="d-inline-block align-top" alt="Armada">
        <a class="navbar-brand" style="margin-left: 5px"; href="/test/armada-web-project/HTML_PHP/index.php">  Armada</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#barrenavigation" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                 <span class="navbar-toggler-icon"></span>
            </button>

        <div class="collapse navbar-collapse" id="barrenavigation">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item <?php echo $state1 ?>"> <a class="nav-link" href="/test/armada-web-project/HTML_PHP/index.php">Acceuil<span class="sr-only">(current)</span></a> </li>
                <li class="nav-item <?php echo $state2 ?>"> <a class="nav-link" href="/test/armada-web-project/HTML_PHP/Vue_Bat.php">Voire Navires</a> </li>
                <li class="nav-item <?php echo $state4 ?>"> <?php echo $respo ?> </li>
                <li class="nav-item <?php echo $state5 ?>"> <?php echo $admin ?> </li>
                <li class="nav-item <?php echo $state3 ?>"> <?php echo $links ?> </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Rechercher sur la page.." aria-label="Reache">
                <button class="btn bnt-sm btn-outline-primary my-2 my-sm-0" type="submit">Rechercher</button>
            </form>
        </div>
       </nav>
    </div>
</header>