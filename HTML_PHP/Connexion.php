<?php session_start(); 
$_SESSION['niv'] = "";
$_SESSION['nom'] = "";
$_SESSION['prenom'] = "";
$_SESSION['id'] = "";
?>

<!DOCTYPE html>
<html lang="fr">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <link rel="stylesheet" href="/test/armada-web-project/CSS/style.css">
      <link href="https://afeld.github.io/emoji-css/emoji.css" rel="stylesheet">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
      <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
      <title>Connexion</title>
  </head>

    <?php
        $state1 = "inactive"; 
        $state2 = "inactive"; 
        $state3 = "active"; 
        $state4 = "inactive"; 
        $state5 = "inactive"; 
        $respo = "";
        $admin = "";
         
        if (empty($_SESSION['nom'])) {
            $links = '<a class="nav-link" href="/test/armada-web-project/HTML_PHP/Connexion.php">Se Connecter</a>';
        } else {
            $links = '<a class="nav-link text-danger" href="/test/armada-web-project/HTML_PHP/Deconnexion.php">Deconnexion</a>';
        }
        if(isset($_SESSION['niv'])){
            switch($_SESSION['niv']){
                case 2:
                    $respo = '<a class="nav-link" href="/test/armada-web-project/HTML_PHP/Editer_Bateau.php">Editer Bateau</a>';
                break;
                case 3:
                    $admin = '<a class="nav-link" href="/test/armada-web-project/HTML_PHP/Admin.php">Consulter droits acces</a>';
                break;
            }
        }
    ?>

    <?php include("header.php"); ?>

  <body>
        <!--Formulaire de connexion-->
    <?php include("traitement_connexion.php"); ?>

    <div id="conn" class="container">

    <?php if(isset($_SESSION['mes'])){
    if($_SESSION['mes'] =="pos"){
        echo '<div class="alert alert-success alert-dismissible fade show" role="alert">';
        echo '<strong>Inscription reussi! <i class="em em-clap"></i></strong> Veuillez vous connecter.';
        echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
        echo '<span aria-hidden="true">&times;</span>';
        echo '</button>';
        echo '</div>';
    }
    if($_SESSION['mes'] =="ncon"){
        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
        echo '<strong>Utilisateur Inexistant! <i class="em em-worried"></i></strong> Veuillez vous inscrire.';
        echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
        echo '<span aria-hidden="true">&times;</span>';
        echo '</button>';
        echo '</div>';
    }}
    
    if(isset($_GET['mes'])){
        if($_GET['mes']=="dcon"){
        echo '<div class="alert alert-success alert-dismissible fade show" role="alert">';
        echo '<strong>Deconnexion reussi!</strong>';
        echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
        echo '<span aria-hidden="true">&times;</span>';
        echo '</button>';
        echo '</div>';
        }
    }?>
        <!--Script PHP pour lenvoie des données de connexion-->
        <form role="form" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
            <div class="form-row" style="padding : 50px;">
            
                <div class="col-md-6" style="padding-left: 0px;">
                    <label for="identifiant">Identifiant</label>
                    <input type="text" class="form-control" id="identifiant" name= "identifiant" value="<?php echo $identifiant; if(isset($_COOKIE['identifiant'])){echo $_COOKIE['identifiant'];}?>" placeholder="Saisissez votre identifiant">
                    <div class="text-danger"><?php echo($errId); ?></div>
                </div>
            
                <div class="col-md-6">
                    <label for="password">Mot de Passe</label>
                    <input type="password" class="form-control" id="password" name="password" value="<?php echo $password; if(isset($_COOKIE['password'])){echo $_COOKIE['password'];}?>" placeholder="Saisissez votre Mot de Passe" >
                    <div class="text-danger"> <?php echo($errPass); ?></div>
                </div>
            
            <div class="form-row" style="margin-top: 30px;">
                <div class="col-md-12">
                    <div class="custom-control custom-checkbox mb-3">
                        <input name = "checkbox" value="remember" type="checkbox" class="custom-control-input" id="remenber" <?php if (isset($_COOKIE['identifiant'])) echo "checked";?>>
                        <label class="custom-control-label" for="remenber">Se souvenir de moi</label>
                    </div>
                    <div class="col">
                        <div><p>Pas encore inscrit? <a href="/test/armada-web-project/HTML_PHP/Inscription.php">Cliquer ici</a> <img src="/test/armada-web-project/Images/decree.gif" width="30" height="30" alt="bateau"> pour passer à l'inscription</p></div>
                    </div>
                        <button type="submit" name ="submit" class="btn btn-primary mb-2">Se Connecter</button>
                </div>
            </div>
        </form>  
    </div>
</body>

<?php include("footer.inc.php"); ?>

</html>
