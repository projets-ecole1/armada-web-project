<?php
    $nom = $prenom = $email = $password = $id = $password_conf = $id_conf = "";
    $errNom = $errPrenom = $errEmail = $errPassword = $errId = $errPassword_conf = $errId_conf = "";
    $niveau = 1;
    $succes = true;
    
    include("param.inc.php");

    if(isset($_POST["submit"])) 
    {
         // Verifie si le nom est bien entré
        if (empty($_POST["nom"])) {
            $errNom = "* Veuillez indiquer le nom!";
            $succes = false;
        } else {
            $nom = test_input($_POST["nom"]);
            // Verifie si les caracteres sont correctes
            if (!preg_match("/^[a-zA-Z ]*$/",$nom)) {
              $errNom = "* Seul les lettres et l'espace sont autorisés"; 
              $succes = false;
            }
        }
        
        if (empty($_POST["prenom"])) {
            $errPrenom = "* Veuillez indiquer le prenom!";
            $succes = false;
        } else {
            $prenom = test_input($_POST["prenom"]);
            // Verifie si les caracteres sont correctes
            if (!preg_match("/^[a-zA-Z ]*$/",$prenom)) {
              $errPrenom = "* Seul les lettres et l'espace sont autorisés"; 
              $succes = false;
            }
        }

        if (empty($_POST["email"])) {
            $errEmail = "* Veuillez indiquer l'addresse e-mail!";
            $succes = false;
        } else {
            $email = test_input($_POST["email"]);
            // Verifie si les caracteres sont correctes
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
              $errEmail = "* Addresse e-mail invalide"; 
              $succes = false;
            }
        }

        if (empty($_POST["id"])) {
            $errId = "* Veuillez indiquer vos identifiants!";
            $succes = false;
        } else {
            $id = test_input($_POST["id"]);
            // Verifie si les caracteres sont correctes
            if (!preg_match("/^[a-zA-Z ]*$/",$id)) {
              $errId = "* Seul les lettres et l'espace sont autorisés"; 
              $succes = false;
            }
        }

        if (empty($_POST["password"])) {
            $errPassword = "* Veuillez indiquer le mot de passe!";
            $succes = false;
        } else {
            $password = test_input($_POST["password"]);
            // Verifie si les caracteres sont correctes
            if (strlen($password) < 8) {
              $errPassword = "* Mot de passe trop court !";
            }
            if (!(preg_match('#^(?=.*[a-z])#', $password))) {
              $errPassword = "* Mot de passe incorrecte"; 
              $succes = false;
            }
        }

        if (empty($_POST["id_conf"])) {
            $errId_conf = "* Veuillez indiquer vos identifiants!";
            $succes = false;
        } else {
            $id_conf = test_input($_POST["id_conf"]);
            // Verifie si les caracteres sont correctes
            if (!($id_conf == $id)) {
              $errId_conf = "* L'identifiant ne correspond pas!"; 
              $succes = false;
            }
        }

        if (empty($_POST["password_conf"])) {
            $errPassword_conf = "* Veuillez indiquer le mot de passe!";
            $succes = false;
        } else {
            $password_conf = test_input($_POST["password_conf"]);
            // Verifie si les caracteres sont correctes
            if (!($password_conf == $password)) {
                $errId_conf = "* Le mot de passe ne correspond pas!"; 
                $succes = false;
            }
        }
    
        if($succes) 
        {
            $conn = mysqli_connect($servername, $username, $mdp, $myDataBaseName); // Create connection
            // Check connection
            if (!$conn){
               die("Connection failed: " . mysqli_connect_error());
            }

            $pwd_conf_hash = md5($password_conf);
            $sql = mysqli_query($conn,"SELECT * FROM utilisateur WHERE identifiant = '$id_conf' AND password = '$pwd_conf_hash'");
            $count = mysqli_num_rows($sql);

            if($count==0){
                $sql = "INSERT INTO utilisateur (nom,prenom,email,identifiant,password,niveau) VALUES ('$nom','$prenom','$email','$id_conf','$pwd_conf_hash',$niveau)";
                if (!mysqli_query($conn, $sql)) {
                echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                }
                
                if($sql){
                    $_SESSION['mes'] = "pos";
                    header("Status: 301 Moved Permanently", false, 301);
                    header('Location:/test/armada-web-project/HTML_PHP/Connexion.php');
                    exit();
                }
            }else{
                $_SESSION['mes'] = "dbl";
                header("Status: 301 Moved Permanently", false, 301);
                header('Location:/test/armada-web-project/HTML_PHP/Inscription.php?mes=dbl');
                exit();
            }   
        }   
    }
 
    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
      }
?> 