<?php
    if(!empty($_FILES["document_mod"]) && $_FILES["document_mod"]["error"] != 4){

        $dossier = "Descriptions/";

        //2- Validation de l'extension
        $extensions_fichier = strtolower(substr(strrchr($_FILES["document_mod"]["name"],'.'),1)); //Releve l'extension du fichier à l'aide de la variable globale en ne retenant que la partie qui suit immediatement le point
        if($extensions_fichier != 'pdf'){
            $errDocument_mod   = " * Vous devez transferer un fichier de type PDF!";
            $success_d = false;
        }
    
        //3-Validation de la taille
        $taille_max = 2097125;
        $taille = filesize($_FILES["document_mod"]["tmp_name"]);
        if($taille > $taille_max){
        $errDocument_mod   = " * La taille du document depasse 2Mo. Veuillez recommencer!";
        $success_d = false;
        }

        $fichiers_mod = basename($_FILES["document_mod"]["name"]); //Recupération du nom du fichier

    }
    else{
        $errDocument_mod   = " * Veuillez renseigner ce champ et choisir un fichier pdf!";
    }

?>