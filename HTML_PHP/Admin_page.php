<?php 
                include("param.inc.php");
                $id_admin = $_SESSION['niv'];
                $conn = mysqli_connect($servername, $username, $mdp, $myDataBaseName); 
                $usr = mysqli_query($conn,"SELECT `id_user`, `nom`, `prenom`, `niveau` FROM `utilisateur`");
                $i = $niv_usr = 0; 
                $activated = "disabled";
            ?> 
            <div class="card border-primary mb-3">
                <div class="card-header">
                    Administrateur : <?php echo $_SESSION['nom']; echo " "; echo $_SESSION['prenom']; ?>
                </div>
                <div class="card-body">
                    <h3 class="card-title">Liste Utilisateurs</h3>
                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nom</th>
                            <th scope="col">Prenom</th>
                            <th scope="col">Niveau Acces</th>
                            <th scope="col">Statut</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php 
                            while($user_ins = mysqli_fetch_array($usr)){
                                $i++;
                                $ide_usr = $user_ins["id_user"];
                                $nom_usr = $user_ins["nom"];
                                $prenom_usr = $user_ins["prenom"];
                                $niv_usr = $user_ins["niveau"];
                                
                                echo'<tr>';
                                echo '   <th scope="row">'.$i.'</th>';
                                echo '   <td>'.$nom_usr.'</td>';
                                echo '   <td>'.$prenom_usr.'</td>';
                                echo '   <td>'.$niv_usr.'</td>';
                                echo '   <td><form method="POST" class="form-inline" action="Admin_mod.php">';
                                echo '   <input type = "hidden" name = "niv_user_id" value = "'.$ide_usr.'" > ';
                                echo '       <select name = "select" class="custom-select" value="'.$niv_usr.'">';
                                echo '         <option value= "1"'; if($niv_usr == 1){echo " selected";} echo'>Inscrit</option>';
                                echo '         <option value= "2"'; if($niv_usr == 2){echo " selected";} echo'>Responsable Bateaux</option>';
                                echo '         <option value= "3"'; if($niv_usr == 3){echo " selected";} echo'>Administrateur</option>';
                                echo '       </select>';
                                echo '   <button type="submit" name ="mod_niv" style = "margin-left: 10px" class="d-none d-sm-block btn btn-success">Valider</button>';
                                echo '   <button type="submit" name ="mod_niv" style = "margin-left: 5px" class="d-block d-sm-none badge badge-success">Ok</button>';               
                                echo '   </form></td>';                              
                                echo'</tr>';   
                            }                  
                        ?>
                        </tbody>
                        
                    </table>
                    <p class="card-text">L'administrateur à le devoir de valider toutes ses modifications pour les enregistrer dans la BDD.</p>
                   
                </div>
            </div>
        </div>