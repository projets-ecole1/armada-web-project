<?php session_start(); ?>
<!DOCTYPE html>
<html lang="fr">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit = no" >
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <link rel="stylesheet" href="/test/armada-web-project/CSS/style.css">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
      <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
      <title>Page de Vue Bateaux</title>
  </head>

  <!--Barre de navigation-->
  <?php
        $state1 = "inactive"; 
        $state2 = "active"; 
        $state3 = "inactive"; 
        $state4 = "inactive"; 
        $state5 = "inactive"; 
        $respo = "";
        $admin = "";
         
        if (empty($_SESSION['nom'])) {
            $links = '<a class="nav-link" href="/test/armada-web-project/HTML_PHP/Connexion.php">Se Connecter</a>';
        } else {
            $links = '<a class="nav-link text-danger" href="/test/armada-web-project/HTML_PHP/Deconnexion.php">Deconnexion</a>';
        }
        if(isset($_SESSION['niv'])){
            switch($_SESSION['niv']){
                case 2:
                    $respo = '<a class="nav-link" href="/test/armada-web-project/HTML_PHP/Editer_Bateau.php">Editer Bateau</a>';
                break;
                case 3:
                    $admin = '<a class="nav-link" href="/test/armada-web-project/HTML_PHP/Admin.php">Consulter droits acces</a>';
                break;
            }
        }
  ?>

  <?php include("header.php"); ?>

   <?php 
          include("param.inc.php");
         
          $conn = mysqli_connect($servername, $username, $mdp, $myDataBaseName); 
          $bat = mysqli_query($conn,"SELECT * FROM bateaux");
          $nom = $pays = $image = $date_arr = $date_dep = $details = "";
          $dossier_img = "Uploads/";
          $i = $taille = $id_respo = 0;  
          
          if(isset($_SESSION['niv'])){
              $id_respo = $_SESSION['niv'];
          }
        ?>

  <body>
        <!--Texte d'acceuil-->
        <div class="container">
            <div class="card-header">
                <nav>
                    <div class="nav nav-tabs card-header-tabs" id="nav-tab" role="tablist">
                      <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#bat" role="tab" aria-controls="nav-home" aria-selected="true">Présentation Simple</a>
                      <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#bat_det" role="tab" aria-controls="nav-profile" aria-selected="false">Présentation Détaillée</a>
                    </div>
                </nav>
            </div>

            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="bat" role="tabpanel" aria-labelledby="nav-home-tab" style="margin-top:10px;"> 
                    <?php include("liste_cartes.php");?>
                </div>

                <div class="tab-pane fade" id="bat_det" role="tabpanel" aria-labelledby="nav-profile-tab" style="margin-top:10px;">
                    <?php 
                    if($_SESSION['niv']==""){
                        echo'<div class="alert alert-info" role="alert">';
                        echo'  <h4 class="alert-heading">Désolé cher visiteur!</h4>';
                        echo'  <p>La présentation détaillée n\'est disponible qu\'aux inscrits. Veuillez vous inscrire ou vous connecter en <a href="/test/armada-web-project/HTML_PHP/Connexion.php">Cliquant ici</a></p>';
                        echo'  <hr>';
                        echo'  <p class="mb-0">Cette page contient les informations détaillées de chaque bateau</p>';
                        echo'</div>';
                    }
                    if($_SESSION['niv'] == 3)
                    {
                    echo '<div style = "margin-top: 10px" class="alert alert-info alert-dismissible fade show" role="alert">';
                    echo '<strong>Désolé! Cette option n\'est pas accessible aux administrateurs</strong>';
                    echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
                    echo '<span aria-hidden="true">&times;</span>';
                    echo '</button>';
                    echo '</div>';

                     }
                    if($_SESSION['niv'] !="" && $_SESSION['niv'] != 3){
                        include("liste_cartes_det.php");
                    }
                    ?>
                </div>
            </div>
        </div>
  </body>

<?php include("footer.inc.php"); ?>