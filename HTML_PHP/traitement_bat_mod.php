<?php
    $nom_bat_mod = $pays_mod = $greement_mod = $date_arr_mod = $date_dep_mod = $fichier_mod = $fichiers_mod = $taille_mod = "";
    $errResearch = $errNom_bat_mod = $errTaille_mod = $errPays_mod = $errGreement_mod = $errDate_arr_mod = $errDate_dep_mod = $errImage_mod = $errDocument_mod = "";
    $success = $success_i = $success_d = true;
    $find = false;
    $id = $test = 0;

    include("param.inc.php");

    if(isset($_POST["search"])) 
    {
        if (empty($_POST["research"])) {
            $errResearch = "* Veuillez indiquer le nom du bateau!";
            $success = false;  
            $test = 1;
        }else{
            $nom = test_input($_POST["research"]);
            $conn = mysqli_connect($servername, $username, $mdp, $myDataBaseName); 
            $bat = mysqli_query($conn,"SELECT * FROM bateaux WHERE nom = '$nom'");
            $count = mysqli_num_rows($bat);

            if($count == 0){
                 $errResearch = "* Ce bateau n'existe pas!";
                 $success = false;
            }else{
                $bateau= mysqli_fetch_assoc($bat);
                $id = $bateau["id_bat"];
                $nom_bat_mod = $bateau["nom"];
                $pays_mod = $bateau["pays"];
                $greement_mod = $bateau["greement"];
                $date_arr_mod = $bateau["date_darrivee"];
                $date_dep_mod = $bateau["date_depart"];
                $fichier_mod = $bateau["image"];
                $fichiers_mod = $bateau["document"];
                $taille_mod = $bateau["taille"];
                $find = true;
            } 
        }
    }

    if(isset($_POST["modify"])) 
    {
         // Traitement des images et des documents au préable
         include("traitement_image_mod.php");

         include("traitement_documents_mod.php");

        // Verifie si le nom est bien entré
        if (empty($_POST["nom_bat_mod"])) {
            $errNom_bat_mod = "* Veuillez indiquer le nom du bateau!";
            $success = false;
        } else {
            $nom_bat_mod = test_input($_POST["nom_bat_mod"]);
            // Verifie si les caracteres sont correctes
            if (!preg_match("/^[a-zA-Z ]*$/",$nom_bat_mod)) {
              $errNom_bat_mod = "* Seul les lettres et l'espace sont autorisés"; 
              $success = false;
            }
        }
        
        if (empty($_POST["taille_mod"])) {
            $errTaille_mod = "* Veuillez indiquer la taille du bateau";
            $success = false;
        } else {
            $taille_mod = $_POST["taille_mod"];
        }

        if (empty($_POST["pays_mod"])) {
            $errPays_mod = "* Veuillez indiquer le pays!";
            $success = false;
        } else {
            $pays_mod = $_POST["pays_mod"];
        }

        if (empty($_POST["greement_mod"])) {
            $errGreement_mod = "* Veuillez renseigner ce champ!";
            $success = false;
        } else {
            $greement_mod = strip_tags($_POST['greement_mod']);
        }

        if (empty($_POST["date_arr_mod"])) {
            $errDate_arr_mod = "* Veuillez indiquer la date d'arrivée!";
            $success = false;
        } else {
            $date_arr_mod = $_POST["date_arr_mod"];
        }

        if (empty($_POST["date_dep_mod"])) {
            $errDate_dep_mod = "* Veuillez indiquer la date de départ!";
            $success = false;
        } else {
            $date_dep_mod = $_POST["date_dep_mod"];
        }

        if($success == true && $success_i == true && $success_d == true && $_FILES["document_mod"]["error"] == 0){
            $fichiers_mod = clean($fichiers_mod); //Remplace tout les caracteres accentués
            $fichiers_mod = preg_replace('/([^.a-z0-9]+)/i','-', $fichiers_mod); //Remplace tout les caracteres non alphanumeriques par '-'
            $envoie = move_uploaded_file($_FILES["document_mod"]["tmp_name"], $dossier.$fichiers_mod);
        }
        else{
            $errDocument_mod  ;
            $success_d = false;
        }

        if($success == true && $success_i == true && $success_d == true && $_FILES["image_mod"]["error"] == 0){
            $fichier_mod = clean($fichier_mod); //Remplace tout les caracteres accentués
            $fichier_mod = preg_replace('/([^.a-z0-9]+)/i', '-', $fichier_mod); //Remplace tout les caracteres non alphanumeriques par '-'
            $envoie = move_uploaded_file($_FILES["image_mod"]["tmp_name"], $dossier2.$fichier_mod);
        }else{
            $errImage_mod  = " * Erreur d'enregistrement de l'image!";
            $success_i = false;
        }

        if($success == true && $success_i == true && $success_d == true) 
        {
            $conn = mysqli_connect($servername, $username, $mdp, $myDataBaseName); // Create connection
            // Check connection
            if (!$conn){
               die("Connection failed: " . mysqli_connect_error());
            }

            $request = "UPDATE bateaux SET nom='$nom_bat_mod',pays='$pays_mod',taille='$taille_mod',greement='$greement_mod',date_depart='$date_dep_mod',date_darrivee='$date_arr_mod',image='$fichier_mod',document='$fichiers_mod' WHERE id_bat='$id'";
            if (!mysqli_query($conn, $request)) {
            echo "Error: " . $request . "<br>" . mysqli_error($conn);
            }

            if($request == true){

            echo '<div class="alert alert-success alert-dismissible fade show" role="alert">';
            echo '<strong>Bateau Modifié!</strong>!';
            echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
            echo '<span aria-hidden="true">&times;</span>';
            echo '</button>';
            echo '</div>';

            header("Status: 301 Moved Permanently", false, 301);
            header('Location:/test/armada-web-project/HTML_PHP/Editer_Bateau.php?tab=m');
            exit();  
            
            }
        }   
    }
 
?> 