<?php
    if(!empty($_FILES["document"]) && $_FILES["document"]["error"] != 4){
        //1-Verifie si le fichier exixste 
        $dossier = "Descriptions/";
        $target_file = $dossier. basename($_FILES["document"]["name"]);
        if (file_exists($target_file)) {
            $errDocument = " * Le fichier existe deja!";
            $success_d = false;
        }

        //2- Validation de l'extension
        $extensions_fichier = strtolower(substr(strrchr($_FILES["document"]["name"],'.'),1)); //Releve l'extension du fichier à l'aide de la variable globale en ne retenant que la partie qui suit immediatement le point
        if($extensions_fichier != 'pdf'){
            $errDocument = " * Vous devez transferer un fichier de type PDF!";
            $success_d = false;
        }
    
        //3-Validation de la taille
        $taille_max = 2097125;
        $taille = filesize($_FILES["document"]["tmp_name"]);
        if($taille > $taille_max){
        $errDocument = " * La taille du document depasse 2Mo. Veuillez recommencer!";
        $success_d = false;
        }

        $fichier = basename($_FILES["document"]["name"]); //Recupération du nom du fichier

    }
    else{
        $errDocument = " * Veuillez renseigner ce champ et choisir un fichier pdf!";
    }

?>