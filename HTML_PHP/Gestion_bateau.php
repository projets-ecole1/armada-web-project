            
            <?php $dis = "";
            if($find == false){
                $dis = "disabled";
                    if($test==1){
                    echo '<div style = "margin-top: 10px" class="alert alert-danger alert-dismissible fade show" role="alert">';
                    echo '<strong>Bateau Inexistant!</strong> Veuillez en créer';
                    echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
                    echo '<span aria-hidden="true">&times;</span>';
                    echo '</button>';
                    echo '</div>';
                    }
            }?>
           
            <input type = "hidden" name = "MAX_FILE_SIZE" value = "2097125" > 
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade <?php echo $show_active_c;?>" id="creer" role="tabpanel" aria-labelledby="nav-home-tab" style="margin-top:10px;"> 
                    <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data">
                        <div class="form-row d-flex justify-content-center">
                                <div class="form-group col-md-5">
                                    <label for="nom_bat">Nom</label>
                                    <input type="text" name ="nom_bat" class="form-control" id="nom_bat" value="<?php echo $nom_bat;?>">
                                    <span  class="text-danger"><?php echo $errNom_bat;?></span>
                                </div>

                                <div class="form-group col-md-5">
                                    <label for="taille">Taille</label>
                                    <input type="number" min=20 max= 500 name ="taille" class="form-control" id="taille" value="<?php echo $taille;?>">
                                    <span  class="text-danger"><?php echo $errTaille;?></span>
                                </div>

                                <div class="form-group col-md-10">
                                    <label for="pays">Pays</label>
                                        <select class="custom-select" name = "pays" value="<?php echo $pays;?>">
                                            <?php include("liste_pays.php")?>
                                        <select>
                                    <span  class="text-danger"><?php echo $errPays;?></span>
                                </div>

                                <div class="form-group col-md-10">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Descrition</span>
                                        </div>
                                        <textarea name ="greement" class="form-control" id="greement" value="<?php echo $greement;?>"></textarea>
                                    </div>
                                    <span  class="text-danger"><?php echo $errGreement;?></span>
                                </div>

                                <div class="form-group col-md-5">
                                    <label for="date_arr">Date d'arrivée</label>
                                    <input type="date" name ="date_arr" class="form-control" id="date_arr" value="<?php echo $date_arr;?>">
                                    <span  class="text-danger"><?php echo $errDate_arr;?></span>
                                </div>

                                <div class="form-group col-md-5">
                                    <label for="date_dep">Date de Départ</label>
                                    <input type="date" name="date_dep" id="date_dep" class="form-control" value="<?php echo $date_dep;?>">
                                    <span  class="text-danger"><?php echo $errDate_dep;?></span>
                                </div>

                                <div class="form-group col-md-5">
                                    <label for="image">Image</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text" id="input">Image</span>
                                        </div>
                                        <div class="custom-file">
                                          <input type="file" name ="image" class="custom-file-input" id="image" value="<?php echo $image;?>" accept="image/*">
                                          <label class="custom-file-label" for="image">--Choisir un fichier--</label>
                                        </div>
                                    </div>
                                    <span  class="text-danger"><?php echo $errImage;?></span>
                                </div>

                                <div class="form-group col-md-5">
                                    <label for="document">Document</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text" id="input">Document</span>
                                        </div>
                                        <div class="custom-file">
                                          <input type="file" name ="document" class="custom-file-input" id="document" value="<?php echo $document;?>" accept=".pdf">
                                          <label class="custom-file-label" for="document">--Choisir un fichier--</label>                                          
                                        </div>
                                    </div>
                                    <span  class="text-danger"><?php echo $errDocument;?></span>
                                </div>  

                                <script>
                                    $('.custom-file-input').on('change', function() {
                                        let fileName = $(this).val().split('\\').pop();
                                        $(this).siblings('.custom-file-label').addClass('selected').html(fileName);
                                    });
                                </script>

                            </div>
                            <p class="text-center"> <button type="submit" name = "create" class="btn btn-success">Creer Bateau</button> </p> 
                    </form>
                </div>
                                    
                <div class="tab-pane fade <?php echo $show_active_m;?>" id="mod" role="tabpanel" aria-labelledby="mod-tab" style="margin-top:10px;">
                    <div class="form-row d-flex justify-content-center">
                    <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" class="form-inline my-2 my-lg-0" style = "padding-top: 10px; padding-bottom: 10px">
                      
                        <button class="btn btn-outline-primary my-2 my-sm-0" name = "search" type="submit">Rechercher</button>
                        <input class="form-control mr-sm-2" type="text" placeholder="Nom bu Bateau" name="research" style="margin-left: 5px;" aria-label="Search">  
                        <span  class="text-danger"><?php echo $errResearch;?></span>
                      
                    </form>
                    </div>

                    <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data">
                        <div class="form-row d-flex justify-content-center">
                                <div class="form-group col-md-5">
                                    <label for="nom_bat_mod">Nom</label>
                                    <input type="text" name ="nom_bat_mod" class="form-control" id="nom_bat_mod" value="<?php echo $nom_bat_mod;?>" <?php echo $dis;?>>
                                    <span  class="text-danger"><?php echo $errNom_bat_mod;?></span>
                                </div>

                                <div class="form-group col-md-5">
                                    <label for="taille_mod">Taille</label>
                                    <input type="number" min="30" max="500" name ="taille_mod" class="form-control" id="taille_mod" value="<?php echo $taille_mod;?>" <?php echo $dis;?>>
                                    <span  class="text-danger"><?php echo $errTaille_mod;?></span>
                                </div>

                                <div class="form-group col-md-10">
                                    <label for="pays_mod">Pays</label>
                                        <select class="custom-select" name = "pays_mod" value="<?php echo $pays_mod;?>" <?php echo $dis;?>>
                                            <?php include("liste_pays_mod.php")?>
                                        <select>
                                    <span  class="text-danger"><?php echo $errPays_mod;?></span>
                                </div>

                                <div class="form-group col-md-10">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Descrition</span>
                                        </div>
                                        <textarea name ="greement_mod" class="form-control" id="greement_mod" <?php echo $dis;?>></textarea>
                                        <script>document.getElementById("greement_mod").value = "<?php echo $greement_mod;?>";</script>
                                    </div>
                                    
                                    <span  class="text-danger"><?php echo $errGreement_mod;?></span>
                                </div>

                                <div class="form-group col-md-5">
                                    <label for="date_arr_mod">Date d'arrivée</label>
                                    <input type="date" name ="date_arr_mod" class="form-control" id="date_arr_mod" value="<?php echo $date_arr_mod;?>" <?php echo $dis;?>>
                                    <span  class="text-danger"><?php echo $errDate_arr_mod;?></span>
                                </div>

                                <div class="form-group col-md-5">
                                    <label for="data_dep_mod">Date de Départ</label>
                                    <input type="date" name="date_dep_mod" id="date_dep_mod" class="form-control" value="<?php echo $date_dep_mod;?>" <?php echo $dis;?>>
                                    <span  class="text-danger"><?php echo $errDate_dep_mod;?></span>
                                </div>

                                <div class="form-group col-md-5">
                                    <label for="image_mod">Image</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text" id="input">Image</span>
                                        </div>
                                        <div class="custom-file">
                                          <input type="file" name ="image_mod" class="custom-file-input" id="image_mod" accept="image/*" <?php echo $dis;?>>
                                          <label class="custom-file-label" for="image_mod">--Choisir fichier--</label>
                                        </div>
                                    </div>
                                    <span  class="text-danger"><?php echo $errImage_mod;?></span>
                                </div>

                                <div class="form-group col-md-5">
                                    <label for="document_mod">Document</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text" id="input">Document</span>
                                        </div>
                                        <div class="custom-file">
                                          <input type="file" name ="document_mod" class="custom-file-input" id="document_mod" accept=".pdf" <?php echo $dis;?>>
                                          <label class="custom-file-label" for="document_mod">--Choisir fichier--</label>    
                                                                                
                                        </div>
                                    </div>
                                    <span  class="text-danger"><?php echo $errDocument_mod;?></span>
                                </div>  
                                
                                <script>
                                    $('.custom-file-input').on('change', function() {
                                        let fileName = $(this).val().split('\\').pop();
                                        $(this).siblings('.custom-file-label').addClass('selected').html(fileName);
                                    });
                                </script>
                            </div>

                            <p class="text-center"> <button type="submit" name = "modify" class="btn btn-outline-success" <?php echo $dis;?>>Modifier</button> </p> 
                    </form>
                </div>

                <div class="tab-pane fade <?php echo $show_active_d;?>" id="del" role="tabpanel" aria-labelledby="del-tab" style="margin-top:10px;">
                
                <?php 
                    include("param.inc.php");
                    $id_respo = $_SESSION['id'];
                    $conn = mysqli_connect($servername, $username, $mdp, $myDataBaseName); 
                    $bat = mysqli_query($conn,"SELECT * FROM bateaux WHERE id_respo = '$id_respo'");
                    $nom = $pays;
                    $i = $taille = 0; 
                ?> 

                    <div class="card mb-3">
                        <div class="card-header">
                            Responsable Bateau : <?php echo $_SESSION['nom']; echo " "; echo $_SESSION['prenom']; ?>
                        </div>
                        <div class="card-body">
                            <h3 class="card-title">Liste Bateaux</h3>
                            <table class="table">
                                <thead>
                                  <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nom</th>
                                    <th scope="col">Pays</th>
                                    <th scope="col">Taille</th>
                                  </tr>
                                </thead>
                                <tbody>
                                <?php 
                                    while($bateau= mysqli_fetch_array($bat)){
                                        $i++;
                                        $ide = $bateau["id_bat"];
                                        $nom = $bateau["nom"];
                                        $pays = $bateau["pays"];
                                        $taille = $bateau["taille"];
 
                                    echo'<tr>';
                                    echo'    <th scope="row">'.$i.'</th>';
                                    echo'    <td>'.$nom.'</td>';
                                    echo'    <td>'.$pays.'</td>';
                                    echo'    <td>'.$taille.' m</td>';
                                    echo'    <td><a href="/test/armada-web-project/HTML_PHP/Respo_del.php?var='.$ide.'" class="d-none d-sm-block btn btn-danger" style="padding-left:0px; padding-right:0px;">Supprimer</a></td>';
                                    echo'    <td><a href="/test/armada-web-project/HTML_PHP/Respo_del.php?var='.$ide.'" class="d-block d-sm-none badge badge-danger" style="margin-right: 10px;">X</a></td>';
                                    echo'</tr>';

                                    }                                       
                                ?>
                                </tbody>
                            </table>
                            <?php 
                            if($i == 0){
                                echo '<div class="card-text">';
                                echo '<div class="alert alert-info alert-dismissible fade show" role="alert">';
                                echo '<strong>Vous n\'avez aucun bateau enregistré en votre nom!</strong>';
                                echo '</div>';
                                echo '</div>';
                            }
                            ?>
                        </div>
                    </div>
                </div>

            </div>